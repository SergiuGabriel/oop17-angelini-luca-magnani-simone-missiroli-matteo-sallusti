package utility;

/**
 * @author Simone
 *the Enum with the possibility of Input.
 */
public enum PlayerType { 
    /**
     * bar set with player input.
     */
    PLAYER, 
    /**
     * no bar.
     */
    EMPTY,
    /**
     * bar set with player AI_easy.
     */
    AI_EASY, 
    /**
     * bar set with player AI_medium.
     */
    AI_MEDIUM, 
    /**
     * bar set with player AI_hard.
     */
    AI_HARD;

}
