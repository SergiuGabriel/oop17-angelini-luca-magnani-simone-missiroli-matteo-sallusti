package utility;

/**
 * @author Simone
 * the Enum with the elements existing in this game.
 */
public enum GraphicType {
    /** 
     * a bar.
     */
    BAR, 
    /**
     * a ball.
     */
    BALL, 
    /**
     * a PowerUp.
     */
    PU,
}
